/*!
 * \file  byteDeque.h
 * \brief  A byte buffer allowing to extend to front and back.
 *         The interface is VERY loosly based on C++ std::deque
 *
 * \copyright  johwech <https://gitlab.com/johwech>
 */
/*
 * SPDX-License-Identifier: MIT
 *
 * MIT License
 *
 * Copyright (c) 2022 johwech
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef BYTEDEQUE_H_
#define BYTEDEQUE_H_

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>


//! \defgroup push_clear_method Push Clear Method
//! Options to configure the clearance behaviour when a push of data is done on a queue that still
//! has sufficient space in the internal buffer to accomodate the new data, but the space is not
//! available on the desired side of the buffer.
//!
//! @{

//! No automatic clearing, a push with insufficient space will lead to an error
#define BYTEDEQUE_PUSHCLEAR_NONE 1

//! The stored data is shifted exactly as far as needed to accomoate the requested push.
//! After the push command, there will be 0 empty storage elements on pushed side.
#define BYTEDEQUE_PUSHCLEAR_FIT 2

//! The stored data is shifted fully to the other end of the internal buffer so any empty storage
//! elements are on the side the last push command was issued on.
//! Corresponds to doing a byteDeque_moveToFront for a byteDeque_pushBack or a byteDeque_moveToBack
//! for a byteDeque_pushFront.
#define BYTEDEQUE_PUSHCLEAR_FULL 3

//! @}

//! Select the push_clear_method for the \ref byteDeque_pushFront function.
#if !defined(BYTEDEQUE_PUSHFRONTCLEAR)
#define BYTEDEQUE_PUSHFRONTCLEAR BYTEDEQUE_PUSHCLEAR_FIT
#endif

//! Select the \ref push_clear_method for the \ref byteDeque_pushBack function.
#if !defined(BYTEDEQUE_PUSHBACKCLEAR)
#define BYTEDEQUE_PUSHBACKCLEAR BYTEDEQUE_PUSHCLEAR_FIT
#endif

#ifdef __cplusplus
extern "C" {
#endif

//! Instance of a double ended queue.
//! The fields shall not be written directly to. Before usage of a queue byteDeque_init needs to be
//! called
struct byteDeque_Handle {
	uint8_t * data;  //!< Pointer to the actual data storage pointer
	int front_idx;  //!< Array index of the first written storage element from the front
	int back_idx;  //!< Array index of the first available storage element from the back
	size_t max_size;  //!< Maximum capacity of data
	size_t size;  //!< Number of bytes written
};

/*!
 * \brief  Initialize the queue before the first use
 *
 * \param[in,out]  h  Pointer to the hande to initialize
 * \param[in]  buffer  Buffer that will be used by the byte double ended queue, this cannot be used
 *                     while the queue is also in use
 * \param[in]  bufferSize  Size of the given buffer
 *
 * \return  True if the buffer was configured successfully, false otherwise.
 */
bool byteDeque_init(struct byteDeque_Handle * h, uint8_t * buffer, size_t bufferSize);

/*!
 * \brief  Presets or replaces the content with the given data at the desired offset from the front
 *
 * \param[in,out]  h  Pointer to the handle
 * \param[in]  src  Data that will be the sole content of the queue after this operation succeeds
 * \param[in]  src_size  Size of the given data in number of elements
 * \param[in]  front_offset  Number of empty elements in front of data after this operation
 *
 * \return  True if the data could be stored in the queue, false otherwise.
 */
bool byteDeque_swap(struct byteDeque_Handle * h, uint8_t const * src, size_t src_size, size_t front_offset);

/*!
 * \brief  Copys the content to the front of the internal buffer, making room at the back
 *
 * \param[in,out]  h  Pointer to the handle
 * \param[in]  front_offset  Additional empty elements in front of data after this operation
 *
 * \return  True if the relocation was successful, false otherwise.
 */
bool byteDeque_moveToFront(struct byteDeque_Handle * h, size_t front_offset);

/*!
 * \brief  Copys the content to the back of the internal buffer, making room at the front
 *
 * \param[in,out]  h  Pointer to the handle
 * \param[in]  back_offset  Additional empty elements in front of data after this operation
 *
 * \return  True if the relocation was successful, false otherwise.
 */
bool byteDeque_moveToBack(struct byteDeque_Handle * h, size_t back_offset);

/*!
 * \brief  Prepends additional data in front of the first element of the already stored content
 *
 * \param[in,out]  h  Pointer to the handle
 * \param[in]  src  Data that will be the sole content of the queue after this operation succeeds
 * \param[in]  src_size  Size of the given data in elements
 *
 * \return  True if the data was added, false otherwise.
 */
bool byteDeque_pushFront(struct byteDeque_Handle * h, uint8_t const * src, size_t src_size);

/*!
 * \brief  Appendss additional data to the back of the last element of the already stored content
 *
 * \param[in,out]  h  Pointer to the handle
 * \param[in]  src  Data that will be the sole content of the queue after this operation succeeds
 * \param[in]  src_size  Size of the given data in elements
 *
 * \return  True if the data was added, false otherwise.
 */
bool byteDeque_pushBack(struct byteDeque_Handle * h, uint8_t const * src, size_t src_size);

/*!
 * \brief  Reads out and removes a given number of of elements from the front of the stored content
 *
 * \param[in,out]  h  Pointer to the handle
 * \param[in]  dest  Destination wherte the popped elements will be written to
 * \param[in]  dest_size  Number of elements the destination can hold at most
 * \param[in]  pop_size  Number of elements that should be popped
 *
 * \return  True if the data was successfully retrieved, false otherwise.
 */
bool byteDeque_popFront(struct byteDeque_Handle * h, uint8_t * dest, size_t dest_size, size_t pop_size);

/*!
 * \brief  Reads out and removes a given number of of elements from the back of the stored content
 *
 * \param[in,out]  h  Pointer to the handle
 * \param[in]  dest  Destination wherte the popped elements will be written to
 * \param[in]  dest_size  Number of elements the destination can hold at most
 * \param[in]  pop_size  Number of elements that should be popped
 *
 * \return  True if the data was successfully retrieved, false otherwise.
 */
bool byteDeque_popBack(struct byteDeque_Handle * h, uint8_t * dest, size_t dest_size, size_t pop_size);

/*!
 * \brief  Returns a pointer to the first element of the content.
 *
 * Please use the appropiate push or pop functions to add or remove data.
 * *Any changes to the length of the content will result in data corruption.*
 *
 * \param[in]  h  Pointer to the handle
 *
 * \return  True if the data was successfully retrieved, false otherwise.
 */
static inline uint8_t * byteDeque_getFront(struct byteDeque_Handle * h) {
	return &h->data[h->front_idx];
}

/*!
 * \brief  Returns the number of empty elements in the internal buffer, before the first element
 *         of the content.
 *
 * After a push operation, the space in front can vary drastically from what you'd expect depending
 * on what BYTEDEQUE_PUSHFRONTCLEAR and BYTEDEQUE_PUSHBACKCLEAR are set to.
 *
 * \param[in]  h  Pointer to the handle
 *
 * \return  Number of empty elements in front of the stored content
 */
static inline size_t byteDeque_getSpaceFront(struct byteDeque_Handle const * h) {
	if(h->size == 0) return h->max_size;
	return h->front_idx;
}

/*!
 * \brief  Returns the number of empty elements in the internal buffer, after the last element
 *         of the content.
 *
 * After a push operation, the space in front can vary drastically from what you'd expect depending
 * on what BYTEDEQUE_PUSHFRONTCLEAR and BYTEDEQUE_PUSHBACKCLEAR are set to.
 *
 * \param[in]  h  Pointer to the handle
 *
 * \return  Number of empty elements behind the last element the stored content
 */
static inline size_t byteDeque_getSpaceBack(struct byteDeque_Handle const * h) {
	if(h->size == 0) return h->max_size;
	return h->max_size - h->back_idx;
}

/*!
 * \brief  Returns the total number of elements the buffer can still hold
 *
 * \param[in]  h  Pointer to the handle
 *
 * \return  Number of elements the buffer can still hold
 */
static inline size_t byteDeque_getSpaceTotal(struct byteDeque_Handle const * h) {
	return h->max_size - h->size;
}

/*!
 * \brief  Returns the number of occupied elements in the buffer
 *
 * \param[in]  h  Pointer to the handle
 *
 * \return  Number of elements the buffer can still hold
 */
static inline size_t byteDeque_getSize(struct byteDeque_Handle const * h) {
	return h->size;
}

#ifdef __cplusplus
}
#endif

#endif
