/*!
 * \file  byteDeque.c
 * \brief  A byte buffer allowing to extend to front and back.
 *
 * \copyright  johwech <https://gitlab.com/johwech>
 */
/*
 * SPDX-License-Identifier: MIT
 *
 * MIT License
 *
 * Copyright (c) 2022 johwech
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <byte_deque/byteDeque.h>
#include <string.h>
#include <assert.h>
#include <limits.h>

bool byteDeque_init(struct byteDeque_Handle * h, uint8_t * buffer, size_t bufferSize) {
	if(h == NULL) return false;
	if(buffer == NULL) return false;
	if(bufferSize < 1) return false;
	if(bufferSize >= INT_MAX) return false;

	// assign underlying buffer
	h->data = buffer;
	h->max_size = bufferSize;

	// set indices to invalid value
	h->front_idx = INT_MIN;
	h->back_idx = INT_MIN;
	h->size = 0;

	return true;
}

bool byteDeque_moveToFront(struct byteDeque_Handle * h, size_t offset) {
	if((offset + h->size) > h->max_size) return false;
	memmove(&h->data[offset], &h->data[h->front_idx], h->size);
	h->back_idx = h->size + offset;
	h->front_idx = offset;
	return true;
}

bool byteDeque_moveToBack(struct byteDeque_Handle * h, size_t offset) {
	if((offset + h->size) > h->max_size) return false;
	memmove(&h->data[h->max_size - h->size - offset], &h->data[h->front_idx], h->size);
	h->back_idx = h->max_size - offset;
	h->front_idx = h->max_size - h->size - offset;
	return true;
}

bool byteDeque_pushBack(struct byteDeque_Handle * h, uint8_t const * src, size_t src_size) {
	// check if there is still enough space in total
	if(h->max_size < (h->size + src_size) ) return false;

	// check if there is enough space to push, if not handle as given by define
	if(byteDeque_getSpaceBack(h) < src_size) {
		#if BYTEDEQUE_PUSHBACKCLEAR == BYTEDEQUE_PUSHCLEAR_NONE
			return false;
		#elif BYTEDEQUE_PUSHBACKCLEAR == BYTEDEQUE_PUSHCLEAR_FIT
			byteDeque_moveToBack(h, src_size);
		#elif BYTEDEQUE_PUSHBACKCLEAR == BYTEDEQUE_PUSHCLEAR_FULL
			byteDeque_moveToFront(h, 0);
		#endif
	}

	// at the start the indices are not valid yet
	if(h->size == 0) {
		h->back_idx = h->max_size - src_size;
		h->front_idx = h->max_size - src_size;
	}

	memcpy(&h->data[h->back_idx], src, src_size);
	h->back_idx += src_size;
	h->size += src_size;
	return true;
}

bool byteDeque_pushFront(struct byteDeque_Handle * h, uint8_t const * src, size_t src_size) {
	// check if there is still enough space in total
	if(h->max_size < (h->size + src_size) ) return false;

	// at the start the indices are not valid yet
	if(h->size == 0) h->back_idx = src_size;

	// check if there is enough space to push, if not handle as given by define
	if(byteDeque_getSpaceFront(h) < src_size) {
		#if BYTEDEQUE_PUSHFRONTCLEAR == BYTEDEQUE_PUSHCLEAR_NONE
			return false;
		#elif BYTEDEQUE_PUSHFRONTCLEAR == BYTEDEQUE_PUSHCLEAR_FIT
			byteDeque_moveToFront(h, src_size);
		#elif BYTEDEQUE_PUSHFRONTCLEAR == BYTEDEQUE_PUSHCLEAR_FULL
			byteDeque_moveToBack(h, 0);
		#endif
	}

	// at the start the indices are not valid yet
	if(h->size == 0) {
		h->back_idx = src_size;
		h->front_idx = src_size;
	}

	int new_front_idx = h->front_idx - src_size;
	memcpy(&h->data[new_front_idx], src, src_size);
	h->front_idx = new_front_idx;
	h->size += src_size;
	return true;
}

bool byteDeque_popFront(struct byteDeque_Handle * h, uint8_t * dest, size_t dest_size, size_t pop_size) {
	// check if enough data was pushed before
	if(h->size < pop_size) return false;
	if(pop_size > dest_size) return false;

	memcpy(dest, &h->data[h->front_idx], pop_size);
	h->front_idx += pop_size;
	h->size -= pop_size;
	return true;
}

bool byteDeque_popBack(struct byteDeque_Handle * h, uint8_t * dest, size_t dest_size, size_t pop_size) {
	// check if enough data was pushed before
	if(h->size < pop_size) return false;
	if(pop_size > dest_size) return false;

	memcpy(dest, &h->data[h->back_idx - pop_size], pop_size);
	h->back_idx -= pop_size;
	h->size -= pop_size;
	return true;
}

bool byteDeque_swap(struct byteDeque_Handle * h, uint8_t const * src, size_t src_size, size_t front_offset) {
	// check if src can fit into buffer including offset
	if(h->max_size < src_size + front_offset) return false;

	h->front_idx = front_offset;
	h->back_idx = h->front_idx + src_size;
	h->size = src_size;
	memcpy(&h->data[h->front_idx], src, src_size);

	return true;
}
