/*!
 * \file  testByteDeque_basics.cpp
 * \brief  Basic unittests for the byteDequeue. These should suffice to reach 100% coverage.
 *
 * \copyright  johwech <https://gitlab.com/johwech>
 */
/*
 * SPDX-License-Identifier: MIT
 *
 * MIT License
 *
 * Copyright (c) 2022 johwech
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <doctest/doctest.h>
#include <byte_deque/byteDeque.h>

#include "testByteDeque.hpp"

#include <vector>
#include <numeric>
#include <climits>

TEST_SUITE_BEGIN("basics");

TEST_CASE("Init: Invalid arugments arguments") {
	uint8_t buffer[10] = {0};
	struct byteDeque_Handle bdh = {};

    CHECK_FALSE( byteDeque_init(nullptr, buffer, sizeof(buffer)) );
	CHECK_FALSE( byteDeque_init(&bdh, nullptr, sizeof(buffer)) );
	CHECK_FALSE( byteDeque_init(&bdh, buffer, 0) );
	CHECK_FALSE( byteDeque_init(&bdh, buffer, INT_MAX+1) );
}

TEST_CASE("Valid init Invalid push / pop calls") {
	uint8_t buffer[10] = {0};
	struct byteDeque_Handle bdh = {};
	std::vector<uint8_t> content((sizeof(buffer) / 2));
	std::iota(content.begin(), content.end(), 1);

	REQUIRE( byteDeque_init(&bdh, buffer, sizeof(buffer)) );
	REQUIRE( byteDeque_getSpaceTotal(&bdh) == sizeof(buffer) );
	REQUIRE( byteDeque_getSize(&bdh) == 0);
	REQUIRE( byteDeque_swap(&bdh, content.data(), content.size(), 0) );
	REQUIRE( byteDeque_getSize(&bdh) == content.size() );

	SUBCASE("Push too much data") {
		size_t space_total = byteDeque_getSpaceTotal(&bdh);
		std::vector<uint8_t> data(space_total + 1);
		SUBCASE("pushFront") {
			CHECK_FALSE( byteDeque_pushFront(&bdh, data.data(), data.size()) );
		}

		SUBCASE("pushBackt") {
			CHECK_FALSE( byteDeque_pushBack(&bdh, data.data(), data.size()) );
		}
	}

	SUBCASE("Pop more than in content") {
		// create destination buffer smaller than the pop size
		size_t size = byteDeque_getSize(&bdh);
		size_t pop_size = size + 1;
		std::vector<uint8_t> dest(pop_size);

		SUBCASE("pop") {
			CHECK_FALSE( byteDeque_popFront(&bdh, dest.data(), dest.size(), pop_size) );
		}

		SUBCASE("pop") {
			CHECK_FALSE( byteDeque_popBack(&bdh, dest.data(), dest.size(), pop_size) );
		}
	}

	SUBCASE("Pop dest too small") {
		// create destination buffer smaller than the pop size
		int pop_size = content.size();
		int dest_size = content.size() - 1;
		std::vector<uint8_t> dest(dest_size);

		SUBCASE("popFront") {
			CHECK_FALSE( byteDeque_popFront(&bdh, dest.data(), dest.size(), pop_size) );
		}

		SUBCASE("popBack") {
			CHECK_FALSE( byteDeque_popBack(&bdh, dest.data(), dest.size(), pop_size) );
		}
	}
}

TEST_CASE("Pop - Invalid destination sizes") {
	uint8_t buffer[10] = {0};
	struct byteDeque_Handle bdh = {};
	std::vector<uint8_t> content(sizeof(buffer) / 2);
	std::iota(content.begin(), content.end(), 1);

	REQUIRE( byteDeque_init(&bdh, buffer, sizeof(buffer)) );
	REQUIRE( byteDeque_getSpaceTotal(&bdh) == sizeof(buffer) );
	REQUIRE( byteDeque_swap(&bdh, content.data(), content.size(), 0) );

	// create destination buffer smaller than the pop size
	int pop_size = content.size();
	int dest_size = content.size() - 1;
	std::vector<uint8_t> dest(dest_size);

	SUBCASE("Front") {
		CHECK_FALSE( byteDeque_popFront(&bdh, dest.data(), dest.size(), pop_size) );
	}

	SUBCASE("Back") {
		CHECK_FALSE( byteDeque_popBack(&bdh, dest.data(), dest.size(), pop_size) );
	}
}

TEST_CASE("Push and pop valid") {
	uint8_t buffer[10] = {0};
	struct byteDeque_Handle bdh = {};

	REQUIRE( byteDeque_init(&bdh, buffer, sizeof(buffer)) );
	REQUIRE( byteDeque_getSpaceTotal(&bdh) == sizeof(buffer) );
	REQUIRE( byteDeque_getSize(&bdh) == 0 );

	std::vector<uint8_t> out(sizeof(buffer));
	std::vector<uint8_t> in(sizeof(buffer));
	std::iota(in.begin(), in.end(), 1);

	SUBCASE("Complete buffer push-pop: single push front and pop front") {
		CHECK( byteDeque_pushFront(&bdh, in.data(), in.size()) );
		CHECK( byteDeque_getSize(&bdh) == in.size() );
		VERIFY_SPACE_IS_0(bdh);
		CHECK( byteDeque_popFront(&bdh, out.data(), out.size(), out.size()) );
		VERIFY_SPACE_IS_MAX(bdh);
	}

	SUBCASE("Complete buffer push-pop: single push back and pop back") {
		CHECK( byteDeque_pushBack(&bdh, in.data(), in.size()) );
		CHECK( byteDeque_getSize(&bdh) == in.size() );
		VERIFY_SPACE_IS_0(bdh);
		CHECK( byteDeque_popFront(&bdh, out.data(), out.size(), out.size()) );
		VERIFY_SPACE_IS_MAX(bdh);
	}

	SUBCASE("Complete buffer push-pop: single push back and pop front") {
		CHECK( byteDeque_pushBack(&bdh, in.data(), in.size()) );
		CHECK( byteDeque_getSize(&bdh) == in.size() );
		VERIFY_SPACE_IS_0(bdh);
		CHECK( byteDeque_popFront(&bdh, out.data(), out.size(), out.size()) );
		VERIFY_SPACE_IS_MAX(bdh);
	}

	SUBCASE("Complete buffer push-pop: single push front and pop back") {
		CHECK( byteDeque_pushFront(&bdh, in.data(), in.size()) );
		CHECK( byteDeque_getSize(&bdh) == in.size() );
		VERIFY_SPACE_IS_0(bdh);
		CHECK( byteDeque_popBack(&bdh, out.data(), out.size(), out.size()) );
		VERIFY_SPACE_IS_MAX(bdh);
	}

	SUBCASE("Complete buffer push-pop: bytewise push front and bytewise pop back") {
		// for pushing the space in front and back depend on BYTEDEQUE_PUSHCLEARBACK and
		// BYTEDEQUE_PUSHCLEARFRONT, in this test only check total space
		for(auto byte_in: in) {
			int space_total = byteDeque_getSpaceTotal(&bdh);
			int size = byteDeque_getSize(&bdh);
			CHECK( byteDeque_pushFront(&bdh, &byte_in, 1) );
			CHECK( byteDeque_getSpaceTotal(&bdh) == (space_total - 1) );
			CHECK( byteDeque_getSize(&bdh) == (size + 1) );
		}
		for(auto &byte_out: out) {
			CHECK( byteDeque_getSpaceFront(&bdh) == 0);
			int space_back = byteDeque_getSpaceBack(&bdh);
			int size = byteDeque_getSize(&bdh);
			CHECK( byteDeque_popBack(&bdh, &byte_out, 1, 1) );
			CHECK( byteDeque_getSpaceBack(&bdh) == (space_back + 1) );
			CHECK( byteDeque_getSize(&bdh) == (size - 1) );
		}
	}

	SUBCASE("Complete buffer push-pop: bytewise push back and bytewise pop front") {
		// for pushing the space in front and back depend on BYTEDEQUE_PUSHCLEARBACK and
		// BYTEDEQUE_PUSHCLEARFRONT, in this test only check total space
		for(auto byte_in: in) {
			int space_total = byteDeque_getSpaceTotal(&bdh);
			int size = byteDeque_getSize(&bdh);
			CHECK( byteDeque_pushBack(&bdh, &byte_in, 1) );
			CHECK( byteDeque_getSpaceTotal(&bdh) == space_total - 1);
			CHECK( byteDeque_getSize(&bdh) == (size + 1) );
		}
		for(auto &byte_out: out) {
			CHECK( byteDeque_getSpaceBack(&bdh) == 0);
			int space_front = byteDeque_getSpaceFront(&bdh);
			int size = byteDeque_getSize(&bdh);
			CHECK( byteDeque_popFront(&bdh, &byte_out, 1, 1) );
			CHECK( byteDeque_getSpaceFront(&bdh) == space_front + 1);
			CHECK( byteDeque_getSize(&bdh) == (size - 1) );
		}
	}

	CHECK( byteDeque_getSize(&bdh) == 0 );

	VERIFY_INT_VECTORS(in, out);
}

TEST_CASE("Swap") {
	uint8_t buffer[16] = {0};
	struct byteDeque_Handle bdh = {};

	REQUIRE( byteDeque_init(&bdh, buffer, sizeof(buffer)) );
	REQUIRE( byteDeque_getSpaceTotal(&bdh) == sizeof(buffer) );

	std::vector<uint8_t> swap(sizeof(buffer) / 2);
	std::iota(swap.begin(), swap.end(), 1);
	std::vector<uint8_t> out(swap.size());
	int front_offset_max = (sizeof(buffer) - swap.size());

	SUBCASE("All valid offsets") {
		for(int front_offset = 0; front_offset <= front_offset_max; front_offset++) {
			CHECK( byteDeque_swap(&bdh, swap.data(), swap.size(), front_offset) );
			CHECK( byteDeque_getSpaceTotal(&bdh) == (sizeof(buffer) - swap.size()) );
			CHECK( byteDeque_getSpaceFront(&bdh) == front_offset );
			CHECK( byteDeque_getSpaceBack(&bdh) == (sizeof(buffer) - swap.size() - front_offset) );
			CHECK( byteDeque_popFront(&bdh, out.data(), out.size(), swap.size()) );
			VERIFY_INT_VECTORS(swap, out);
		}
	}

	SUBCASE("Invalid offset") {
		int front_offset = front_offset_max + 1;
		CHECK_FALSE( byteDeque_swap(&bdh, swap.data(), swap.size(), front_offset) );
	}

	SUBCASE("Multiple Swaps") {
		std::vector<uint8_t> swap2(swap.size());
		std::iota(swap2.begin(), swap2.end(), swap.size() + 1);

		int swap2_front_offset_max = (sizeof(buffer) - swap2.size());

		CHECK( byteDeque_swap(&bdh, swap.data(), swap.size(), 0) );
		CHECK( byteDeque_swap(&bdh, swap2.data(), swap2.size(), swap2_front_offset_max) );
		CHECK( byteDeque_getSpaceTotal(&bdh) == (sizeof(buffer) - swap2.size()) );
		CHECK( byteDeque_popBack(&bdh, out.data(), out.size(), swap2.size()) );
		VERIFY_INT_VECTORS(swap2, out);
		VERIFY_SPACE_IS_MAX(bdh);
	}
}

TEST_CASE("Get Front") {
	uint8_t buffer[16] = {0};
	struct byteDeque_Handle bdh = {};

	REQUIRE( byteDeque_init(&bdh, buffer, sizeof(buffer)) );
	REQUIRE( byteDeque_getSpaceTotal(&bdh) == sizeof(buffer) );

	std::vector<uint8_t> swap(sizeof(buffer) / 2);
	std::iota(swap.begin(), swap.end(), 1);
	std::vector<uint8_t> out(swap.size());
	int front_offset_max = (sizeof(buffer) - swap.size());

	SUBCASE("All valid offsets") {
		for(int front_offset = 0; front_offset <= front_offset_max; front_offset++) {
			CHECK( byteDeque_swap(&bdh, swap.data(), swap.size(), front_offset) );
			CHECK( byteDeque_getFront(&bdh) == &buffer[byteDeque_getSpaceFront(&bdh)]);
		}
	}
}

TEST_CASE("Move") {
	uint8_t buffer[16] = {0};
	struct byteDeque_Handle bdh = {};

	REQUIRE( byteDeque_init(&bdh, buffer, sizeof(buffer)) );
	REQUIRE( byteDeque_getSpaceTotal(&bdh) == sizeof(buffer) );

	std::vector<uint8_t> swap(sizeof(buffer) / 2);
	std::iota(swap.begin(), swap.end(), 1);
	int offset_max = (sizeof(buffer) - swap.size());
	REQUIRE( byteDeque_swap(&bdh, swap.data(), swap.size(), offset_max / 2) );

	SUBCASE("Move Front") {
		for(int offset = 0; offset < offset_max; offset++) {
			CHECK( byteDeque_moveToFront(&bdh, offset) );
			CHECK( byteDeque_getSpaceBack(&bdh) == (offset_max - offset) );
			CHECK( byteDeque_getSpaceFront(&bdh) == offset );
		}
	}

	SUBCASE("MOVE Back") {
		for(int offset = 0; offset < offset_max; offset++) {
			CHECK( byteDeque_moveToBack(&bdh, offset) );
			CHECK( byteDeque_getSpaceFront(&bdh) == (offset_max - offset) );
			CHECK( byteDeque_getSpaceBack(&bdh) == offset );
		}
	}

	SUBCASE("Invalid") {
		size_t pre_move_front_space = byteDeque_getSpaceFront(&bdh);
		size_t pre_move_back_space = byteDeque_getSpaceBack(&bdh);

		SUBCASE("Front") {
			CHECK_FALSE( byteDeque_moveToFront(&bdh, offset_max+1) );
		}

		SUBCASE("Back") {
			CHECK_FALSE( byteDeque_moveToBack(&bdh, offset_max+1) );
		}

		CHECK( byteDeque_getSpaceFront(&bdh) == pre_move_front_space );
		CHECK( byteDeque_getSpaceBack(&bdh) == pre_move_back_space );
		std::vector<uint8_t> out(swap.size());
		CHECK( byteDeque_popBack(&bdh, out.data(), out.size(), swap.size()) );
		VERIFY_INT_VECTORS(swap, out);
	}
}


TEST_SUITE_END();
