/*!
 * \file  testByteDeque_random.cpp
 * \brief  Testing a random set of push and pop operations on randomly choses buffer sizes.
 *
 * \copyright  johwech <https://gitlab.com/johwech>
 */
/* SPDX-License-Identifier: MIT
 *
 * MIT License
 *
 * Copyright (c) 2022 johwech
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <doctest/doctest.h>
#include <byte_deque/byteDeque.h>
#include "testByteDeque.hpp"

#include <vector>
#include <numeric>
#include <ranges>
#include <algorithm>
#include <deque>

TEST_SUITE_BEGIN("random");

TEST_CASE("Random: Random test against std::deque") {
	//! Maximum buffersize that can be used in every instanciation
	constexpr int buffersize_max = 16384;

	//! Number of different instanciations are tested
	constexpr int n_buffer_sizes = 128;

	//! Number of randomly chosen operations
	constexpr int n_operations = 1024;

	std::srand(0);
	for(int n = 0; n < n_buffer_sizes; n++) {
		CAPTURE(n);

		// create instaces with random sizes
		int buffer_size = (std::rand() % (buffersize_max - 1)) + 1;

		std::deque<uint8_t> deque;

		std::vector<uint8_t> buffer(buffer_size);
		struct byteDeque_Handle bdh = {};
		byteDeque_init(&bdh, buffer.data(), buffer_size);

		for(int i = 0; i < n_operations; i++) {

			// get random number with at most buffer size
			int chunk_size = (std::rand() % buffer_size) + 1;

			// determine whether to push or pop
			bool push = (std::rand() % 2);

			// select fron or back
			bool front = (std::rand() % 2);

			if(push) {
				std::vector<uint8_t> in(chunk_size, 0);
				std::generate(in.begin(), in.end(), std::rand);

				if(front) {
					if(deque.size() + chunk_size > (size_t) buffer_size) {
						CHECK_FALSE( byteDeque_pushFront(&bdh, in.data(), in.size()) );
					}
					else {
						// push in reverse to have the same behaviour to have the chunks in correct order
						for (auto i = in.rbegin(); i != in.rend(); ++i ) {
							deque.push_front(*i);
						}
						CHECK( byteDeque_pushFront(&bdh, in.data(), in.size()) );
					}
				}
				else {
					if(deque.size() + chunk_size > (size_t) buffer_size) {
						CHECK_FALSE( byteDeque_pushBack(&bdh, in.data(), in.size()) );
					}
					else {
						for(auto byte: in) {
							deque.push_back(byte);
						}
						CHECK( byteDeque_pushBack(&bdh, in.data(), in.size()) );
					}
				}
				CHECK( byteDeque_getSize(&bdh) == deque.size() );
				CHECK( byteDeque_getSpaceTotal(&bdh) == (buffer_size - deque.size()) );
			}
			// pop
			else {
				std::vector<uint8_t> out_cpp(chunk_size, 0);
				std::vector<uint8_t> out_c(chunk_size, 0);

				if(front) {
					if(deque.size() < (size_t) chunk_size) {
						CHECK_FALSE( byteDeque_popFront(&bdh, out_c.data(), out_c.size(), out_c.size()) );
					}
					else {
						CHECK( byteDeque_popFront(&bdh, out_c.data(), out_c.size(), out_c.size()) );

						for(auto &byte: out_cpp) {
							byte = deque.front();
							deque.pop_front();
						}
					}
				}
				else {
					if(deque.size() < (size_t) chunk_size) {
						CHECK_FALSE( byteDeque_popBack(&bdh, out_c.data(), out_c.size(), out_c.size()) );
					}
					else {
						CHECK( byteDeque_popBack(&bdh, out_c.data(), out_c.size(), out_c.size()) );

						for (auto i = out_cpp.rbegin(); i != out_cpp.rend(); ++i ) {
							*i = deque.back();
							deque.pop_back();
						}
					}
				}

				VERIFY_INT_VECTORS(out_c, out_cpp);
			}
		}
	}
}

TEST_SUITE_END();
