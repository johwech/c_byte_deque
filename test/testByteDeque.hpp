/*!
 * \file  testByteDeque.hpp
 * \brief  Helper functions and macros to test the byteDeque with doctest.
 *
 * \copyright  johwech <https://gitlab.com/johwech>
 */
/* SPDX-License-Identifier: MIT
 *
 * MIT License
 *
 * Copyright (c) 2022 johwech
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef TESTBYTEDEQUE_HPP_
#define TESTBYTEDEQUE_HPP_

#include <doctest/doctest.h>

#include <vector>
#include <sstream>
#include <iterator>

#define VERIFY_SPACE_IS_0(bdh)  {              \
	CHECK( byteDeque_getSpaceFront(&bdh) == 0); \
	CHECK( byteDeque_getSpaceBack(&bdh) == 0);  \
	CHECK( byteDeque_getSpaceTotal(&bdh) == 0); \
}

#define VERIFY_SPACE_IS_MAX(bdh)  {                       \
	CHECK( byteDeque_getSpaceFront(&bdh) == bdh.max_size); \
	CHECK( byteDeque_getSpaceBack(&bdh) == bdh.max_size);  \
	CHECK( byteDeque_getSpaceTotal(&bdh) == bdh.max_size); \
}

#define VERIFY_INT_VECTORS(a,b)  {                                             \
	if(a != b) {                                                               \
		std::stringstream a_ss;                                                \
		std::copy(a.begin(), a.end(), std::ostream_iterator<long>(a_ss, " ")); \
		std::stringstream b_ss;                                                \
		std::copy(b.begin(), b.end(), std::ostream_iterator<long>(b_ss, " ")); \
		std::string a_str = a_ss.str();                                        \
		std::string b_str = b_ss.str();                                        \
		CHECK(a_str == b_str);                                                 \
	}                                                                          \
}

#endif
